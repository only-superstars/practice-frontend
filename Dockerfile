FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY dist/frontend/ /usr/share/nginx/html
RUN chmod -R 777 /var/log/nginx /var/cache/nginx/ /var/run/ \ 
&& chmod 644 /etc/nginx/*
CMD nginx -g "daemon off;"


