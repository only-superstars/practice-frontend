#script:
#read all properties from property-file
while read line
do
COLON=`expr index "$line" ':'`
PROPERTY=${line::$COLON-1}
VALUE=${line:$COLON}
case "$PROPERTY" in
"SERVICE_NAME") SERVICE_NAME=$VALUE;;
"CLOUD_NAMESPACE") CLOUD_NAMESPACE=$VALUE;;
"OPENSHIFT_CLOUD") OPENSHIFT_CLOUD=$VALUE;;
"DOCKER_REGISTRY_USERNAME") DOCKER_REGISTRY_USERNAME=$VALUE;;
"OCLOGIN") OCLOGIN=$VALUE;;
"BUILD_NUMBER") BUILD_NUMBER=$VALUE;;
esac
done <script/properties.yaml
echo "$SERVICE_NAME $CLOUD_NAMESPACE $OPENSHIFT_CLOUD $DOCKER_REGISTRY_USERNAME $OCLOGIN $BUILD_NUMBER"


#1. Build image
echo "Build an image"
REPOSITORY=${DOCKER_REGISTRY_USERNAME}/${SERVICE_NAME}
docker build -t ${REPOSITORY}:build${BUILD_NUMBER} .

echo "Push image to registry"
REPOSITORY=${DOCKER_REGISTRY_USERNAME}/${SERVICE_NAME}
TAG="build${BUILD_NUMBER}"
DOCKER_BUILD_PATH=${REPOSITORY}:${TAG}
docker push $DOCKER_BUILD_PATH


#2. Increasing build number

sed -i 's/BUILD_NUMBER.*/BUILD_NUMBER:'$((BUILD_NUMBER + 1))'/' script/properties.yaml


#3.Deploy



oc project $CLOUD_NAMESPACE


oc process -f openshift/openshift.json -p REPOSITORY=$REPOSITORY -p TAG=$TAG -p CLOUD=$OPENSHIFT_CLOUD -n $CLOUD_NAMESPACE | oc apply -f -