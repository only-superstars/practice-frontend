import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { DataSource } from '@angular/cdk/table';
import {MatDialogConfig} from "@angular/material";

export interface AssignedPair {
  id:string;
  subject: Subject;
  teacher: Teacher;
  day: Day;
  time: Time;
  group:Group;
  weekId:string;
  classRoom:ClassRoom;
  endsIn:number;
}
export interface WeekPairs{
  id:string;
  weekPairs:TimePair[];
}
export interface Week {
  id: number;
  weekStart:string;
  weekEnd:string;
}
export interface ClassRoomPair{
  number:number;
  pair:AssignedPair[];
}
export interface Subject {
  id:string;
  name:string;
}
export interface ClassRoom {
  id:string;
  number:number;
  places:number;
}
export interface Day {
  id:string;
  day:string;
}
export interface Time {
  id:string;
  time:string;
}
export interface Group {
  groupNumber:number;
  studentCount:number;
}
export interface Teacher {
  id:string;
  name:string;
}
export interface TimePair {
  time:string;
  day:string;
  pairs:AssignedPair[];
}
export interface DialogData {
  subject: string;
  group: number;
  classroom: number;
  startDate: string;
  weekCount: number;
  teacher: string;
}
@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {
  private addPairUrl:string =  "/addnewPair"
  constructor(private httpClient: HttpClient,
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  addPairRequest(data){
    const httpHeader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    const params = new HttpParams().set('number', data.subject).set('number1', data.group)
    this.httpClient.post(this.addPairUrl, params, { headers: httpHeader}
      ).subscribe(result => {
    });
    this.dialogRef.close();
  }

}
export class AddClassRoomData{
  placeCount:number;
  classRoomNumber:number;
}
@Component({
  selector: 'add-classroom-dialog',
  templateUrl: 'add-classroom-dialog.html',
})
export class AddClassRoomDialog {
  private addNewClassroomURL: string = "/addclassroom";
  constructor( private httpClient: HttpClient,
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: AddClassRoomData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  addNewClassroom(data) {
    this.httpClient.post(this.addNewClassroomURL, {
          number: data.classRoomNumber.toString(),
          places: data.placeCount.toString()}).subscribe(result => {
    });
    this.dialogRef.close();
  }
}
@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class StartComponent implements OnInit, OnChanges {
  classRooms: ClassRoom[] = [];
  classrooms: string[] = [];
  spans = [];
  weeks = [];
  animal: string;
  name: string;
  weekPairs: WeekPairs[] = []
  timePairs: TimePair[] = [];
  weekId:number;
  columnsToDisplay: string[] = [];
  subject: string;
  group: number;
  classroom: number;
  startDate: string;
  weekCount: number;
  teacher: string;
  classRoomNumber: number;
  placeCount: number;
  endsIn:number;
  private classRoomURL: string  = "/classrooms/";
  private timePairByWeekURL: string  = "/getallallpairs/";
  private weekURL: string = "/weeks/";

  addPair(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: {subject: this.subject,group: this.group, classroom: this.classroom, startDate: this.startDate, weekCount: this.weekCount, teacher: this.teacher}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
    });
  }
  addClassRoom(): void {
    const dialogRef = this.dialog.open(AddClassRoomDialog, {
      width: '250px',
      data: {classRoomNumber: this.classRoomNumber,placeCount: this.placeCount}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
    });
  }
  constructor(private httpClient: HttpClient, private ref: ChangeDetectorRef, public dialog: MatDialog) {
    this.weekId=11;
    this.endsIn = 2;
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.ref.detectChanges();
  }
  isEndingSoon(endsIn): boolean{
    return this.endsIn >= endsIn;
}
  prevWeek(){
    this.weekId --;
    this.func();
  }
  nextWeek(){
    this.weekId ++;
    this.func();
  }
  getRowSpan(col, index) {
    return this.spans[index] && this.spans[index][col];
  }
  getClassRoomPlaces(column):string{
    let classRoom = this.classRooms.find(item => item.number ==column);
    return classRoom.places.toString();
  }
  func(){
    this.updateView();
    this.classrooms = this.getColumns();
    setTimeout(() => {
      this.columnsToDisplay = ['day','Time'].concat(this.classrooms);
      console.log(this.classrooms);
      console.log(this.columnsToDisplay);
      this.cacheSpan('day', d => d.day);
    },1000);
  }
  ngOnInit() {
    this.func();
    this.ref.detectChanges();
  }

  private updateView() {
    this.ref.detectChanges();
    this.getTimePairs().subscribe(
      data => {
        this.timePairs = data.find(item=>item.id==this.weekId.toString()).weekPairs;
        console.log(this.timePairs);
      }
    );
    this.getAllWeeks().subscribe(
      data => {
        this.weeks = data;
      }
    );
  }

  private getClassRooms(): Observable<ClassRoom[]> {
    return this.httpClient.get <ClassRoom[]>(this.classRoomURL);
  }

  private getTimePairs(): Observable<WeekPairs[]> {
    return this.httpClient.get <WeekPairs[]>(this.timePairByWeekURL);
  }
  private getAllWeeks(): Observable<Week[]> {
    return this.httpClient.get <Week[]>(this.weekURL);
  }
  private getColumns(): string[]{
    let classrooms:string[] = [];
    this.getClassRooms().subscribe(
      data => {
        this.classRooms = data;
        this.classRooms.filter(x => {
          classrooms.push(x.number.toString());
        });
      });
    return classrooms;
  }
  cacheSpan(key, accessor) {
    for (let i = 0; i < this.timePairs.length;) {
      let currentValue = accessor(this.timePairs[i]);
      let count = 1;

      // Iterate through the remaining rows to see how many match
      // the current value as retrieved through the accessor.
      for (let j = i + 1; j < this.timePairs.length; j++) {
        if (currentValue != accessor(this.timePairs[j])) {
          break;
        }

        count++;
      }

      if (!this.spans[i]) {
        this.spans[i] = {};
      }

      // Store the number of similar values that were found (the span)
      // and skip i to the next unique row.
      this.spans[i][key] = count;
      i += count;
    }
  }
  testcheck(time1:string , time2:string) {
    return time1 === time2;
  }
  getCurrentWeek(): string{
    let curWeek = this.weeks.find(item=>item.id==this.weekId);
    if(curWeek)
    return "Week from "+ curWeek.weekStart + " to " + curWeek.weekEnd;
    else return ""
  }
}




